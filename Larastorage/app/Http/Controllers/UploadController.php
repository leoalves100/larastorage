<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class UploadController extends Controller
{
    public function formUpload()
    {
        return view('upload');
    }

    public function upload(Request $request)
    {
        //Utilizando o disco do GCS
        $response = Storage::disk('gcs')->put('teste', $request->file, 'private');

        echo json_encode($response, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }   
}
