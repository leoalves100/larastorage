<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;

class DownloadController extends Controller
{
    public function downloadFile(Request $file)
    {   
        return Storage::disk('gcs')->download($file->name);
    }
}