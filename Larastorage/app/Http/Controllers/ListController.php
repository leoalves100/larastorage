<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use function GuzzleHttp\json_encode;

class ListController extends Controller
{
    public function findAll($folder)
    {   
        $disk = Storage::disk('gcs')->files($folder);
        return response()->json($disk, 200); 
    }

    public function find(Request $id)
    {
        $url = Storage::disk('gcs')->url($id->name);

        echo json_encode(['link' => $url], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }

    public function filesAll()
    {
        $files = Storage::disk('gcs')->allFiles();
        
        return response()->json(['link' => $files], 200,);        
    }

    public function urlTest()
    {   
        $disk = Storage::disk('gcs')->allFiles();

        // foreach ($disk as $files) {
        //     $url[] = Storage::disk('gcs')->url($files);
        // }
        
        return view('link')->withLink($disk);
    }
}
