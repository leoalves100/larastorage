<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class DeleteController extends Controller
{
    public function deleteFile(Request $request)
    {
        $response = Storage::disk('gcs')->delete($request->name);

        if ($response) {
            echo json_encode(['message' => 'Arquivo deletado com sucesso'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        } else {
            echo json_encode(['message' => 'Arquivo não existe'], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
    }  
}
