<?php
#HOME
Route::get('/', 'UploadController@formUpload');

#UPLOAD 
Route::post('/enviar', 'UploadController@upload');

#LISTAR
#Lista por pasta
Route::get('/listar/{folder}', 'ListController@findAll');

#Lista por id do arquivo
Route::get('/file', 'ListController@find');

#Lista todos os arquivos do bucket
Route::get('/bucket', 'ListController@filesAll');

#Teste de urls
Route::get('/testurl', 'ListController@urlTest');

#DOWNLOAD
Route::get('/download', 'DownloadController@downloadFile');

#DELETAR
Route::get('/delete', 'DeleteController@deleteFile');