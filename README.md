# Trabalhando com Google Cloud Storage

> API portada para o framework laravel, utilizando pacote externo para comunicação com o Cloud Storage.

&nbsp; 

# Informações do ambiente de desenvolvimento

* Laravel v5.8
* Apache v2.4.39
* MariaDB v10.3.16
* PHP v7.3.7 (VC15 X86 64bit thread safe) + PEAR
* XAMPP Control Panel Version 3.2.4.

### Dependências necessárias

* [laravel-google-cloud-storage](https://github.com/Superbalist/laravel-google-cloud-storage)

### Documentação
**📖 [Wiki](https://gitlab.com/abitat.live/playground/larastorage/wikis/home)**


&nbsp;

## [0.1] 2019-09-02
### Changed
+ Download de arquivos
+ Listagem 
    + Arquivos
    + Buckets
+ Upload

## [0.2] 2019-09-04

### Fixed
+ JSON exibido sem formatação

### Added   
+ Deletar arquivos

&nbsp;



### Referências
[Documentation Laravel](https://laravel.com/docs/5.8/filesystem)